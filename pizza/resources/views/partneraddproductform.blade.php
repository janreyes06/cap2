@extends('template')
@section('title', 'Partner Page | Add car')
@section('body')

	 <nav class="navbar sticky-top navbar-expand-lg navbar-dark my-navbar">
		<a href="/catalog" class="navbar-brand animated jackInTheBox"> 
			<img src="{{ asset('images/icons8-f-48.png') }}">
			<span class="land-title-style my-own-logo">Fast & Luxurious
		</a>

		<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-nav">
			<span class="land-title-style">F</span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			
			<ul class="navbar-nav mx-auto">
				<li class="nav-item active-nav-item">
					<a href="/catalog" class="nav-link">Cars List</a>
				</li>
				<li class="nav-item">
					<a href="/cart" class="nav-link">Transactions</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/partner/add" class="nav-link active">Be our Partner</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/userorderhistory" class="nav-link">Transaction History</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<li class="nav-item nav-sign-in-link">
					<a href="/home" class="nav-link">
						Home
					</a>
				</li>
			</ul>

		</div>
	</nav>
	<!-- end nav -->

	<div class="container partneradd">
		<p style="color: red;">*Wait for our approval after sending us your unit!</p>
		<div class="row">
			<div class="col-md-8 mx-auto mt-3">

				{{-- Validation Error response --}}
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<form action="/partnerproduct/form/add" method="POST" enctype="multipart/form-data">
					@csrf

					<label>Car name:</label>
					<input type="text" name="partner_product_name" class="form-control">

					<label>Price:</label>
					<input type="number" name="partner_product_price" class="form-control">

					<label>Description:</label>
					<textarea name="partner_product_description" class="form-control"></textarea>

					<label>Category:</label>
					<select name="partner_product_category" class="form-control">
						<option value="">Select a category</option>
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->category_name }}</option>
						@endforeach
					</select>

					<label>Upload Image:</label>
					<input type="file" name="partner_product_image" class="form-control">

					<button type="submit" class="btn btn-success btn-block mt-2">Submit</button>

				</form>

			</div>
		</div>
	</div>
@endsection