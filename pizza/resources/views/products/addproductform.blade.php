@extends('template')
@section('title', 'Admin Page | Add Product')

@section('body')

	<nav class="navbar sticky-top navbar-expand-lg navbar-dark my-navbar">
		<a href="/catalog" class="navbar-brand animated jackInTheBox"> 
			<img src="{{ asset('images/icons8-f-48.png') }}">
			<span class="land-title-style my-own-logo">Fast & Luxurious
		</a>

		<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-nav">
			<span class="land-title-style">F</span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			
			<ul class="navbar-nav mx-auto">
				<li class="nav-item active-nav-item">
					<a href="/transactionhistory" class="nav-link">Transactions History</a>
				</li>
				<li class="nav-item">
					<a href="/product/form" class="nav-link active">Add Car</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/productlist" class="nav-link">Car List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/businesspartners" class="nav-link">Partner List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/partnerproduct/list" class="nav-link">Partner's Cars</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<li class="nav-item nav-sign-in-link">
					<a href="/home" class="nav-link">
						Home
					</a>
				</li>
			</ul>

		</div>
	</nav>
	<!-- end nav -->
	
	<div class="container">
		<div class="row">
			<div class="col-md-8 mx-auto mt-3">

				{{-- Validation Error response --}}
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<form action="/product/form/add" method="POST" enctype="multipart/form-data">
					@csrf

					<label>Car name:</label>
					<input type="text" name="product_name" class="form-control">

					<label>Price:</label>
					<input type="number" name="product_price" class="form-control">

					<label>Description:</label>
					<textarea name="product_description" class="form-control"></textarea>

					<label>Category:</label>
					<select name="product_category" class="form-control">
						<option value="">Select a category</option>
						@foreach($categories as $category)
							<option value="{{ $category->id }}">{{ $category->category_name }}</option>
						@endforeach
					</select>

					<label>Status:</label>
					<input type="text" name="product_status" class="form-control">

					<label>Upload Image:</label>
					<input type="file" name="product_image" class="form-control">

					<button type="submit" class="btn btn-success btn-block mt-2">Submit</button>

				</form>

			</div>
		</div>
	</div>






@endsection
	


