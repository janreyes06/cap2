@extends('template')
@section('title', 'Admin Page | Product List')

@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	<nav class="navbar sticky-top navbar-expand-lg navbar-dark my-navbar">
		<a href="/catalog" class="navbar-brand animated jackInTheBox"> 
			<img src="{{ asset('images/icons8-f-48.png') }}">
			<span class="land-title-style my-own-logo">Fast & Luxurious
		</a>

		<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-nav">
			<span class="land-title-style">F</span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			
			<ul class="navbar-nav mx-auto">
				<li class="nav-item active-nav-item">
					<a href="/transactionhistory" class="nav-link">Transactions History</a>
				</li>
				<li class="nav-item">
					<a href="/product/form" class="nav-link">Add Car</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/productlist" class="nav-link active">Car List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/businesspartners" class="nav-link">Partner List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/partnerproduct/list" class="nav-link">Partner's Cars</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<li class="nav-item nav-sign-in-link">
					<a href="/home" class="nav-link">
						Home
					</a>
				</li>
			</ul>

		</div>
	</nav>
	<!-- end nav -->

	<div class="container-fluid">
		<h2 class="text-center my-4">Cars List</h2>
		<p class="text-center"><input id="myInput" type="text" placeholder="Search.."></p>
		<div class="row">
			<div class="col-md-12 mx-auto table-responsive text-center">
				
				<table class="table table-hover">
					
					<thead class="thead-dark">
						<tr>
							<th scope="col">
								<form action="/productlist" method="GET">
									@csrf
									<select name="by_product_name">
										<option value="asc">Asending</option>
										<option value="desc">Descending</option>
									</select>
									<button type="submit" class="btn btn-primary">By name</button>		
								</form>
							</th>
							<th scope="col">Car Rent</th>
							<th scope="col">Car description</th>
							<th scope="col">Status</th>
							<th scope="col">Car Image</th>
							<th scope="col">Action</th>
							<th scope="col"></th>
						</tr>
					</thead>

					<tbody id="myTable">
						@foreach($products as $product)
						<tr>
							<td>{{ $product->product_name }}</td>
							<td>{{ $product->price }}</td>
							<td>{{ $product->description }}</td>
							<td>{{ $product->estado }}</td>
							<td><img src="{{ $product->image }}"></td>
							<td>		
								{{-- <a href="/product/delete/{{ $product->id }}" class="btn btn-danger mt-1">Delete</a> --}}
								<button class="deleteProductButton btn btn-danger mt-1" data-id="{{ $product->id }}">Delete</button>			
							</td>
							<td>
								<a href="/product/form/update/{{ $product->id }}" class="btn btn-success mt-1">Update</a> 
							</td>
						</tr>
						@endforeach()
					</tbody>


				</table>


			</div>
		</div>
	</div>

<script type="text/javascript">
		
		let deleteButtons = document.querySelectorAll('.deleteProductButton');

		const CSRFTOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

		deleteButtons.forEach(function(deleteButton) {
			
			let productID = deleteButton.getAttribute("data-id");
			
			let productTobeRemoved = deleteButton.parentElement.parentElement;


			deleteButton.addEventListener("click", function(){

				fetch('/product/delete/'+productID, {
					method: "get",
					// body: 
					headers: {'X-CSRF-TOKEN' : CSRFTOKEN }
				})
				
				.then(function(responseFromOurServer){
					
					return responseFromOurServer.text();
				})
				.then(function(theResponseThatWillBeReturnToOurPage){
					productTobeRemoved.remove();
				})
			});

		});

	</script>



@endsection