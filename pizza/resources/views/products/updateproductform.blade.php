@extends('template')
@section('title', 'Admin Page | Update Product')
@section('body')
	
	<div class="container">
		<div class="row">
			<div class="col-md-8 mx-auto">
				
				<form action="/product/form/update/{{ $product->id }}" method="POST" enctype="multipart/form-data">
					@csrf
					@method('PATCH')
					{{-- @dd($product) --}}
					<label>Product name:</label>
					<input type="text" name="product_name" class="form-control" value="{{ $product->product_name }}">

					<label>Price:</label>
					<input type="number" name="product_price" class="form-control" value="{{ $product->price }}">

					<label>Description:</label>
					<textarea name="product_description" class="form-control">
						{{ $product->description }}
					</textarea>

					<label>Category:</label>
					<select name="product_category" class="form-control">
						<option value="">Select a category</option>

						
						
						@foreach($categories as $category)
							@if($product->category_id == $category->id)
								<option value="{{ $category->id }}" selected>{{ $category->category_name }}</option>
							@else
								<option value="{{ $category->id }}">{{ $category->category_name }}</option>
							@endif
						@endforeach
					</select>

					<label>Status:</label>
					<input type="text" name="product_status" class="form-control" value="{{ $product->estado }}">

					<label>Upload Image:</label>
					<input type="file" name="product_image" class="form-control">

					<button type="submit" class="btn btn-primary mt-3">Update Product</button>
					<a href="/productlist" class="btn btn-danger mt-3">Cancel</a>
				</form>

			</div>
		</div>
	</div>


@endsection
	
