@extends('template')
@section('title', 'Fast & Luxurious | My Transaction')
@section('body')

	<nav class="navbar sticky-top navbar-expand-lg navbar-dark my-navbar">
		<a href="/catalog" class="navbar-brand animated jackInTheBox"> 
			<img src="{{ asset('images/icons8-f-48.png') }}">
			<span class="land-title-style my-own-logo">Fast & Luxurious
		</a>

		<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-nav">
			<span class="land-title-style">F</span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			
			<ul class="navbar-nav mx-auto">
				<li class="nav-item active-nav-item">
					<a href="/catalog" class="nav-link">Cars List</a>
				</li>
				<li class="nav-item">
					<a href="/cart" class="nav-link active">Transactions</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/partner/add" class="nav-link">Be our Partner</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/userorderhistory" class="nav-link">Transaction History</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<li class="nav-item nav-sign-in-link">
					<a href="/home" class="nav-link">
						Home
					</a>
				</li>
			</ul>

		</div>
	</nav>
	<!-- end nav -->
	
	<div class="container-fluid mt-5">
		<div class="row">
			<div class="col-md-10 mx-auto table-responsive">
			@if(session('message'))
				<div class="alert alert-primary" role="alert">
					{{ session('message') }}
				</div>
			@endif
				{{-- {{ $total }}
				@dd($product_cart) --}}
			@if($product_cart != null)
				<table class="table">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Product</th>
							<th scope="col">No. of days</th>
							<th scope="col">Price</th>
							<th scope="col">Subtotal</th>
							<th scope="col">Action</th>
						</tr>
					</thead>

					<tbody>
						@foreach($product_cart as $product)
							<tr>
								<td>{{ $product->product_name }}</td>
								<td>
									<form action="/cart/{{ $product->id }}" method="POST">
										@csrf
										@method('PUT')
										<input type="number" name="newQuantity" value="{{ $product->quantity }}">
										<button type="submit" class="btn btn-success">Update</button>
									</form>
									

								</td>
								<td>{{ $product->price }}</td>
								<td>{{ $product->subtotal }}</td>
								<td>
									<form action="/cart/{{ $product->id }}" method="POST">
										@csrf
										@method('DELETE')
										<button class="btn btn-danger" type="submit">Delete</button>
									</form>
								</td>
							</tr>
						@endforeach

						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>{{ $total }}</td>
							<td>
								<form action="/orders" method="POST">
									@csrf
									<select name="payment_mode" id="">
										@foreach($payment_modes as $payment_mode)
										<option value="{{ $payment_mode->id }}">{{ $payment_mode->payment_mode_name }}</option>
										@endforeach
									</select>
									<button type="submit" class="btn btn-success">Check out</button>
								</form>
							</td>							
						</tr>

						<tr>
							<td>
								<a href="/clearcart" class="btn btn-danger">Cancel Transaction</a>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				@else
					<div class="jumbotron mt-4 text-center mt-5">
						<h1 class="display-4">You don't have any transactions!</h1>
						<p class="lead">
							Rent a Fast & Luxurious Car!
						</p>
						<a href="/catalog" class="btn btn-success btn-lg">Check out some Cars!</a>
					</div>
				@endif
			</div>
		</div>
	</div>

@endsection