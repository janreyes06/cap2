@extends('template')
@section('title', 'Admin Page | Business partners')

@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	<nav class="navbar sticky-top navbar-expand-lg navbar-dark my-navbar">
		<a href="/catalog" class="navbar-brand animated jackInTheBox"> 
			<img src="{{ asset('images/icons8-f-48.png') }}">
			<span class="land-title-style my-own-logo">Fast & Luxurious
		</a>

		<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-nav">
			<span class="land-title-style">F</span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			
			<ul class="navbar-nav mx-auto">
				<li class="nav-item active-nav-item">
					<a href="/transactionhistory" class="nav-link">Transactions History</a>
				</li>
				<li class="nav-item">
					<a href="/product/form" class="nav-link">Add Car</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/productlist" class="nav-link">Car List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/businesspartners" class="nav-link active">Partner List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/partnerproduct/list" class="nav-link">Partner's Cars</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<li class="nav-item nav-sign-in-link">
					<a href="/home" class="nav-link">
						Home
					</a>
				</li>
			</ul>

		</div>
	</nav>
	<!-- end nav -->

	<div class="container-fluid">
		<h2 class="text-center my-4">Business Partners</h2>
		<p class="text-center"><input id="myInput" type="text" placeholder="Search.."></p>
		<div class="row">
			<div class="col-md-11 mx-auto tabe-responsive text-center">
				<table class="table table-hover">
					
					<thead class="thead-dark">
						<th scope="col">Name</th>
						<th scope="col">Email</th>
						<th scope="col">Car Name</th>
						<th scope="col">Price</th>
						<th scope="col">Description</th>
						<th scope="col">Image</th>
					</thead>

					<tbody id="myTable">
						@foreach($partners as $partner)
						<tr>
							<td>{{ $partner->name }}</td>
							<td>{{ $partner->user->email }}</td>
							<td>{{ $partner->product_name }}</td>
							<td>{{ $partner->price }}</td>
							<td>{{ $partner->description }}</td>
							<td><img src="{{ asset($partner->image) }}"></td>
						</tr>
						@endforeach
					</tbody>



				</table>
			</div>
		</div>
	</div>
@endsection