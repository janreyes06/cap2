@extends('template')

@section('title', 'Fast & Luxurious | Catalog Page')

@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable .card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	<!-- navbar -->
	<nav class="navbar sticky-top navbar-expand-lg navbar-dark my-navbar">
		<a href="/catalog" class="navbar-brand animated jackInTheBox"> 
			<img src="{{ asset('images/icons8-f-48.png') }}">
			<span class="land-title-style my-own-logo">Fast & Luxurious
		</a>

		<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-nav">
			<span class="land-title-style">F</span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			
			<ul class="navbar-nav mx-auto">
				<li class="nav-item active-nav-item my-li-nav-padding">
					<a href="/catalog" class="nav-link active">Cars List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/cart" class="nav-link">Transactions</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/partner/add" class="nav-link">Be our Partner</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/userorderhistory" class="nav-link">Transaction History</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<li class="nav-item nav-sign-in-link">
					<a href="/home" class="nav-link">
						Home
					</a>
				</li>
			</ul>

		</div>
	</nav>
	<!-- end nav -->

	<div class="container-fluid mt-4" id="carlist">
		<p style="color: red;">*Please proceed to "Transactions" to finalize your rent request</p>
		<p class="text-center"><input id="myInput" type="text" placeholder="Search.."></p>
		<div class="row">

				{{-- card container for products --}}
				@foreach($products as $product)
					
					<div class="col-md-4 mt-4" id="myTable">
							
						<div class="card my-card">
						  <img class="card-img-top img-fluid" src="{{ asset($product->image) }}" alt="Card image cap" style="height: 300px; width: auto;">
						  	<div class="card-body">
							    <h5 class="card-title text-center">{{ $product->product_name }}</h5>
							    <p class="card-text"><strong>Rent price:</strong> Php {{ $product->price }}/day</p>
							    <p class="card-text">"{{ $product->description }}"</p> 

							    {{-- add to cart form --}}
							    {{-- Goal: get the product ID and the quantity --}}
							    @if($product->estado != "inuse")
							    <form action="/cart" method="POST" class="form-inline">
							    	@csrf
							    	<input type="number" name="product_id" value="{{ $product->id }}" hidden>
							    	<input type="number" name="quantity" class="form-control text-center mx-auto" placeholder="No. of days">

							    	<button type="submit" class="btn btn-success btn-block mt-1">Rent</button>
							    </form>
							    @else
							    	<h4 class="card-text text-center" style="color: red;">Currently In-Use</h4>
							    @endif

						  	</div>
						</div>

					</div>
				@endforeach
		</div>
	</div>

	<div class="container-fluid my-5" data-aos="fade-right" style="color: white;">
		<div class="row bpartnerrow">
			<div class="col-md-6 text-center bpartnercol1">
				<h2>Be One of Our Business Partners!</h2>
				<p>If you have a cool car that you want to be rented, this is the place for you!</p>
				<a href="/partner/add" class="btn btn-success btn-lg">Proceed</a>
			</div>
			<div class="col-md-6  bpartner" id="image">
				<img class="img-fluid" src="{{ asset('images/1.jpg') }}">
			</div>
		</div>	
	</div>
	
<div class="container my-slider">
	<h2 class="text-center">Some of Our Clients!</h2>
<div class="row">
  <div class="col-md-12">
  <!-- Swiper -->
  <div class="swiper-container" id="myswiper-container">
    <div class="swiper-wrapper">
      
      <div class="swiper-slide" id="myswiper">
        <div class="imgBx">
          <img src="{{ asset('images/2.jpg') }}">
        </div>
        <div class="details">
          <p>"The cars are so good! Helped me to have an idea what car to buy"</p>
        </div>
      </div>

      <div class="swiper-slide" id="myswiper">
        <div class="imgBx">
          <img src="{{ asset('images/3.jpg') }}">
        </div>
        <div class="details">
          <p>"I love the Ferrari California. I look so cool while I'm driving that car"</p>
        </div>
      </div>

      <div class="swiper-slide" id="myswiper">
        <div class="imgBx">
          <img src="{{ asset('images/4.jpg') }}">
        </div>
        <div class="details">
          <p>"The cars are legit! Perfect for people who have some bucketlist out there!"</p>
        </div>
      </div>

      <div class="swiper-slide" id="myswiper">
        <div class="imgBx">
          <img src="{{ asset('images/5.jpg') }}">
        </div>
        <div class="details">
          <p>"Bumblebee is my favorite in the trasformers movie. The Camaro is so cool!"</p>
        </div>
      </div>

      <div class="swiper-slide" id="myswiper">
        <div class="imgBx">
          <img src="{{ asset('images/6.jpg') }}">
        </div>
        <div class="details">
          <p>"Lambos are the best! I recommend you guys to try the Reventon!"</p>
        </div>
      </div>

      <div class="swiper-slide" id="myswiper">
        <div class="imgBx">
          <img src="{{ asset('images/7.jpg') }}">
        </div>
        <div class="details">
          <p>"The cars run smoothly! Specially the Ferrari F12 which is my favorite!"</p>
        </div>
      </div>

      <div class="swiper-slide" id="myswiper">
        <div class="imgBx">
          <img src="{{ asset('images/8.jpg') }}">
        </div>
        <div class="details">
          <p>"The cars are great! I hope that there will be a Ford Mustang soon!"</p>
        </div>
      </div>

      <div class="swiper-slide" id="myswiper">
        <div class="imgBx">
          <img src="{{ asset('images/9.jpg') }}">
        </div>
        <div class="details">
          <p>"I'm so happy that I've tested driving my dream car!"</p>
        </div>
      </div>
      
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
  </div>
</div>
</div>
</div>

<div class="container-fluid my-5" style="color: white;">
		<div class="row fav">

			<div class="col-md-6 bpartner" id="image">
				<img class="img-fluid" src="{{ asset('images/10.jpg') }}">
			</div>
			<div class="col-md-6 text-center favcol">
				<h2 class="mt-5">Try the Most Favorite Car of Our Clients!</h2>
				<h4>Lamborghini Reventon</h4>
				{{-- <a href="/partner/add" class="btn btn-success btn-lg">Proceed</a> --}}
			</div>
			
		</div>	
	</div>






@endsection