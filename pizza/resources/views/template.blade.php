<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title')</title>

  <!-- fav icon -->
    <link rel="icon" href="{{ asset('images/icons8-f-48.png') }}">

	{{-- Bootstrap CDN --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    {{-- Fontawesome --}}
    <script src="https://kit.fontawesome.com/4de2b5cbee.js" crossorigin="anonymous"></script>

    {{-- External CSS --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

    {{-- swiper css --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/swiper.min.css') }}">

    <!-- aos -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

</head>
<body>
	@yield('body')
  {{-- <div class="container-fluid"> --}}
  <!-- footer -->
 {{--  <footer class="footer my-footer">
    <p class="text-center p-3 mb-0">
      All images belong to the original owner |
      <a href="https://www.freeprivacypolicy.com/" target="_blank" class="social-color">Privacy Policy</a> |
      follow us on
      <a href="https://twitter.com/" target="_blank" class="social-color"><i class="fab fa-twitter"></i></a>
      <a href="https://www.facebook.com/" target="_blank" class="social-color"><i class="fab fa-facebook-f"></i></a>
      <a href="https://www.instagram.com/" target="_blank" class="social-color"><i class="fab fa-instagram"></i></a> | Fast & Luxurious &copy; 2020
    </p>
  </footer> --}}
  <!-- end footer -->
  {{-- </div> --}}

    {{-- swiper js --}}
    <script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>

	<!-- jquery lib -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    {{-- external js --}}
    <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

    <!-- aos -->
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

   <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
</body>
</html>
<script>
var swiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 800,
        modifier: 1,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });

AOS.init({
        duration : 1200,
      });
</script>
