@extends('layouts.app')

@section('content')
<div class="container-fluid">
	@if(Auth::user()->role == 0)
    <div class="jumbotron mt-4 text-center mt-5">
        <h1 class="display-4">Check Out Some Cars!</h1>
        <p class="lead">
            Rent a Fast & Luxurious Car!
        </p>
        <a href="/catalog" class="btn btn-success btn-lg">Proceed</a>
    </div>
    @else
    <div class="jumbotron mt-4 text-center mt-5">
        <h1 class="display-4">Welcome Admin!</h1>
        <p class="lead">
            View Your Dashboard!
        </p>
        <a href="/transactionhistory" class="btn btn-success btn-lg">Proceed</a>
    </div>
    @endif
</div>
@endsection
