@extends('template')
@section('title', 'Admin Page | Partners Cars')

@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	<nav class="navbar sticky-top navbar-expand-lg navbar-dark my-navbar">
		<a href="/catalog" class="navbar-brand animated jackInTheBox"> 
			<img src="{{ asset('images/icons8-f-48.png') }}">
			<span class="land-title-style my-own-logo">Fast & Luxurious
		</a>

		<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-nav">
			<span class="land-title-style">F</span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			
			<ul class="navbar-nav mx-auto">
				<li class="nav-item active-nav-item">
					<a href="/transactionhistory" class="nav-link">Transactions History</a>
				</li>
				<li class="nav-item">
					<a href="/product/form" class="nav-link">Add Car</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/productlist" class="nav-link">Car List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/businesspartners" class="nav-link">Partner List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/partnerproduct/list" class="nav-link active">Partner's Cars</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<li class="nav-item nav-sign-in-link">
					<a href="/home" class="nav-link">
						Home
					</a>
				</li>
			</ul>

		</div>
	</nav>
	<!-- end nav -->

	<div class="container-fluid">
		<h2 class="text-center my-4">Business Partners' Cars List</h2><p class="text-center"><input id="myInput" type="text" placeholder="Search.."></p>
		<div class="row">
			<div class="col-md-12 mx-auto table-responsive text-center">
				<table class="table table-hover">
					
					<thead class="thead-dark">
						<th scope="col">Partner Name</th>
						<th scope="col">Email</th>
						<th scope="col">Car Name</th>
						<th scope="col">Price</th>
						<th scope="col">Description</th>
						<th scope="col">Image</th>
						<th scope="col">Action</th>
						<th scope="col"></th>
					</thead>

					<tbody id="myTable">
						@foreach($partners as $partner)
						<tr>
							<td>{{ $partner->name }}</td>
							<td>{{ $partner->user->email }}</td>
							<td>{{ $partner->product_name }}</td>
							<td>{{ $partner->price }}</td>
							<td>{{ $partner->description }}</td>
							<td><img src="{{ asset($partner->image) }}"></td>
							<td>
								<a href="/partnerapprove/{{ $partner->id }}}" class="btn btn-success mt-1">Approve</a>
							</td>
							<td>
								{{-- <a href="/partnerdeleteproduct/{{ $partner->id }}" class="btn btn-danger">Decline</a> --}}
								<button class="deleteProductButton btn btn-danger mt-1" data-id="{{ $partner->id }}">Decline</button>
							</td>
						</tr>
						@endforeach
					</tbody>



				</table>
			</div>
		</div>
	</div>

<script type="text/javascript">
		
		let deleteButtons = document.querySelectorAll('.deleteProductButton');

		const CSRFTOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

		deleteButtons.forEach(function(deleteButton) {
			
			let productID = deleteButton.getAttribute("data-id");
			
			let productTobeRemoved = deleteButton.parentElement.parentElement;


			deleteButton.addEventListener("click", function(){

				fetch('/partnerdeleteproduct/'+productID, {
					method: "get",
					// body: 
					headers: {'X-CSRF-TOKEN' : CSRFTOKEN }
				})
				
				.then(function(responseFromOurServer){
					
					return responseFromOurServer.text();
				})
				.then(function(theResponseThatWillBeReturnToOurPage){
					productTobeRemoved.remove();
				})
			});

		});

	</script>

@endsection