@extends('template')
@section('title', 'Fast & Luxurious | Transaction History')
@section('body')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

	<nav class="navbar sticky-top navbar-expand-lg navbar-dark my-navbar">
		<a href="/catalog" class="navbar-brand animated jackInTheBox"> 
			<img src="{{ asset('images/icons8-f-48.png') }}">
			<span class="land-title-style my-own-logo">Fast & Luxurious
		</a>

		<button class="navbar-toggler" data-toggle="collapse" data-target="#navbar-nav">
			<span class="land-title-style">F</span>
		</button>

		<div id="navbar-nav" class="collapse navbar-collapse">
			
			<ul class="navbar-nav mx-auto">
				<li class="nav-item active-nav-item my-li-nav-padding">
					<a href="/catalog" class="nav-link active">Cars List</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/cart" class="nav-link">Transactions</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/partner/add" class="nav-link">Be our Partner</a>
				</li>
				<li class="nav-item my-li-nav-padding">
					<a href="/userorderhistory" class="nav-link">Transaction History</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				<li class="nav-item nav-sign-in-link">
					<a href="/home" class="nav-link">
						Home
					</a>
				</li>
			</ul>

		</div>
	</nav>
	<!-- end nav -->
	
	<div class="container-fluid">
		<h2 class="text-center my-4">Transaction History</h2>
		<p class="text-center"><input id="myInput" type="text" placeholder="Search.."></p>
		<div class="row">
			<div class="col-md-11 mx-auto table-responsive">
				<table class="table table-hover">
					<thead class="thead-dark">
						
						<th scope="col">
							<form action="/userorderhistory" method="GET">
							@csrf
							<select name="by_purchase_date">
								<option value="asc">Ascending</option>
								<option value="desc">Descending</option>
							</select>
								<button class="btn btn-primary">By Date:</button>
							</form>
						</th>
						<th scope="col">Total Rent Price</th>
						<th scope="col">Rented Car</th>
						<th scope="col">Day/s</th>
					</thead>

					<tbody id="myTable">
						@foreach($orders as $order)

						@if(Auth::user()->id == $order->user_id)
						<tr>
							
							<td>{{ $order->purchase_date->isoFormat('MMMM Do YYYY, h:mm:ss a') }}</td>
							<td>{{ $order->total_price }}</td>
							@foreach($order->products as $product)
								<td>{{ $product->product_name }}</td>
							@endforeach
							@foreach($order->products as $product)
								<td>{{ $product->pivot->quantity }}</td>
							@endforeach

						</tr>
						@endif
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>





@endsection