<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Payment_ModeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_modes')->insert(
        	[
        		[
        			'payment_mode_name' => 'Cash',
        			'created_at' => Carbon::now(),
        			'updated_at' => Carbon::now()
        		],
        		
        		[
        			'payment_mode_name' => 'Online Banking Payment',
        			'created_at' => Carbon::now(),
        			'updated_at' => Carbon::now()
        		]
        	]
        );
    }
}
