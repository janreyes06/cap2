<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user(){
		return $this->belongsTo('App\User');
	}


	/*Multiple orders belongs to one payment_mode*/
	public function payment_mode(){
		return $this->belongsTo('App\Payment_mode');
	}


	/*Multiple orders belongs to one status*/
	public function status(){
		return $this->belongsTo('App\Status');
	}


	/*
		Order to Product - Many to Many
		Multiple orders belongs to many products
	*/
	public function products(){
		return $this->belongsToMany('App\Product')->withPivot('quantity')->withTimestamps();
	}

	protected $dates = ['purchase_date'];
}
