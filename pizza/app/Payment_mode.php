<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_mode extends Model
{
    public function orders(){
		return $this->hasMany('App\Order');
	}
}
